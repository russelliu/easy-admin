package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.BizLeave;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 请假Mapper接口
 *
 * @author mars
 * @date 2024-01-25
 */
public interface BizLeaveMapper extends BasePlusMapper<BizLeave> {

}
