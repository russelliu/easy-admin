package com.mars.module.workflow.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

/**
 * @author Hp
 */
@Data
@ApiModel(description = "获取流程下一节点信息请求")
public class NextStepRequest implements Serializable {
    private static final long serialVersionUID = -2324842263874405807L;

    @NotBlank(message = "流程实例ID不能为空")
    @ApiModelProperty(value = "流程实例ID", required = true)
    private String instanceId;

    @NotNull
    @ApiModelProperty(value = "当前流程操作人", required = true)
    private String assignee;

    @NotNull
    @ApiModelProperty(value = "当前任务ID", required = true)
    private String taskId;

    @ApiModelProperty(value = "流程变量")
    private Map<String, Object> variables;
}
