package com.mars.module.workflow.response;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "审批操作记录")
public class ActHiOpinionResponse {

    @ApiModelProperty(value = "审批意见id")
    private String id;

    @ApiModelProperty(value = "审批操作类型")
    private String type;

    @ApiModelProperty(value = "当前环节")
    private String currentNode;

    @ApiModelProperty(value = "驳回到的节点，驳回直送时，才有该字段")
    private String globalNode;

    @ApiModelProperty(value = "当前环节名称")
    private String currentNodeName;

    @ApiModelProperty(value = "驳回到的节点名称，驳回直送时，才有该字段")
    private String globalNodeName;

    @ApiModelProperty(value = "流程实例id")
    private String procInstId;

    @ApiModelProperty(value = "操作人id")
    private String userId;

    @ApiModelProperty(value = "操作人名称")
    private String nickName;

    @ApiModelProperty(value = "任务id")
    private String taskId;

    @ApiModelProperty(value = "执行id")
    private String executionId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "更新时间")
    private Date lastUpdateTime;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "审批意见")
    private String opinion;

    @ApiModelProperty(value = "附件名称")
    private String attachmentName;

    @ApiModelProperty(value = "附件url")
    private String attachmentUrl;

    @ApiModelProperty(value = "备注")
    private String remark;
}
