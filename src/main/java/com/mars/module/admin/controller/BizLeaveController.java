package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import com.mars.module.workflow.request.ApproveRequest;
import com.mars.module.workflow.request.StartRequest;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.BizLeave;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IBizLeaveService;
import com.mars.module.admin.request.BizLeaveRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * 请假控制层
 *
 * @author mars
 * @date 2024-01-25
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "请假接口管理", tags = "请假接口管理")
@RequestMapping("/admin/bizLeave")
public class BizLeaveController {

    private final IBizLeaveService iBizLeaveService;

    /**
     * 分页查询请假列表
     */
    @ApiOperation(value = "分页查询请假列表")
    @PostMapping("/pageList")
    public R<PageInfo<BizLeave>> pageList(@RequestBody BizLeaveRequest bizLeaveRequest) {
        return R.success(iBizLeaveService.pageList(bizLeaveRequest));
    }

    /**
     * 获取请假详细信息
     */
    @ApiOperation(value = "获取请假详细信息")
    @GetMapping(value = "/query/{id}")
    public R<BizLeave> detail(@PathVariable("id") Long id) {
        return R.success(iBizLeaveService.getById(id));
    }

    /**
     * 新增请假
     */
    @Log(title = "新增请假", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增请假")
    @PostMapping("/add")
    public R<Void> add(@RequestBody BizLeaveRequest bizLeaveRequest) {
        iBizLeaveService.add(bizLeaveRequest);
        return R.success();
    }


    /**
     * 提交请假流程
     */
    @RateLimiter
    @ApiOperation(value = "提交请假审批")
    @PostMapping("/submitProcess")
    public R<Void> submitProcess(@RequestBody StartRequest request) {
        iBizLeaveService.submitProcess(request);
        return R.success();
    }

    /**
     * 完成审批
     */
    @RateLimiter
    @ApiOperation(value = "完成审批")
    @PostMapping("/submitApprove")
    public R<Void> submitApprove(@Validated @RequestBody ApproveRequest request) {
        iBizLeaveService.submitApprove(request);
        return R.success();
    }

    /**
     * 修改请假
     */
    @Log(title = "修改请假", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改请假")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody BizLeaveRequest bizLeaveRequest) {
        iBizLeaveService.update(bizLeaveRequest);
        return R.success();
    }

    /**
     * 删除请假
     */
    @Log(title = "删除请假", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除请假")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iBizLeaveService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
     * 导出请假
     *
     * @param response response
     * @throws IOException IOException
     */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response, @RequestBody BizLeaveRequest bizLeaveRequest) throws IOException {
        List<BizLeave> list = iBizLeaveService.list(bizLeaveRequest);
        ExcelUtils.exportExcel(list, BizLeave.class, "请假信息", response);
    }
}
