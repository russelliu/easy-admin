package com.mars.module.workflow.service;

import com.mars.module.workflow.entity.ActHiOpinion;
import com.mars.module.workflow.response.ActHiOpinionResponse;

import java.util.List;

public interface IActHiOpinionService {

    Integer saveActHiOpinion(ActHiOpinion actHiOpinion);

    List<ActHiOpinion> queryListWithoutStart(ActHiOpinion actHiOpinion);

    ActHiOpinionResponse getRelationRejectOpinion(ActHiOpinion aho);

    List<ActHiOpinion> removeRecallTask(List<ActHiOpinion> actHiOpinions);
}
