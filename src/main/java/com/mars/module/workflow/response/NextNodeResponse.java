package com.mars.module.workflow.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 下一节点信息
 *
 * @author JIANGLIEGANG
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "下一节点信息")
public class NextNodeResponse implements Serializable {
    private static final long serialVersionUID = -8492720836094430796L;

    @ApiModelProperty(value = "节点id")
    public String nodeId;

    @ApiModelProperty(value = "节点名称")
    public String nodeName;

    @ApiModelProperty(value = "拓展属性")
    private Map<String, String> extendMap;


    @ApiModelProperty(value = "节点类型为ParallelGateway（并行网关情况下会有值）")
    private List<NextNodeResponse> nextNodeResult;

    @ApiModelProperty(value = "是否是直接路由节点，默认是")
    private Boolean directFlowNode = true;

    @ApiModelProperty(value = "是否驳回后直送")
    private Boolean directSendAfterReject;

    @ApiModelProperty(value = "判断是否是多实例")
    private Boolean judgeMultiInstance;

    @ApiModelProperty(value = "节点类型 userTask、ParallelGateway CallActivity  EndEvent 后续有拓展再加")
    private String nodeType;


    public NextNodeResponse(String nodeId, String nodeName) {
        this.nodeId = nodeId;
        this.nodeName = nodeName;
    }
}
