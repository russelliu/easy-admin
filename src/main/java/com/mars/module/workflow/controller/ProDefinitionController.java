package com.mars.module.workflow.controller;


import com.mars.common.enums.BusinessType;
import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.framework.annotation.Log;
import com.mars.module.workflow.entity.ProcessDefinition;
import com.mars.module.workflow.request.ListModelRequest;
import com.mars.module.workflow.request.ModelRequest;
import com.mars.module.workflow.request.ProcessDefinitionRequest;
import com.mars.module.workflow.service.IProcessDefinitionService;
import com.mars.module.workflow.service.IProcessModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.repository.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @author Hp
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "流程引擎定义管理", tags = "流程引擎定义管理")
@RequestMapping("/workflow")
public class ProDefinitionController {

    private final IProcessDefinitionService processDefinitionService;

    /**
     * 流程定义列表
     *
     * @param request request
     * @return R<List < ProcessDefinition>>
     */
    @ApiOperation(value = "流程定义列表")
    @PostMapping("/definition/list")
    public R<PageInfo<ProcessDefinition>> list(@RequestBody ProcessDefinitionRequest request) {
        return R.success(processDefinitionService.pageList(request));
    }

    /**
     * 流程定义状态变更
     *
     * @param request request
     * @return R
     */
    @ApiOperation(value = "流程定义状态变更")
    @PostMapping("/definition/suspendOrActiveApply")
    public R<Void> suspendOrActiveApply(@RequestBody ProcessDefinitionRequest request) {
        processDefinitionService.suspendOrActiveApply(request.getId(), request.getSuspendState());
        return R.success();
    }

    /**
     * 根据部署ID查询流程图
     *
     * @param request request
     * @return R<String>
     */
    @ApiOperation(value = "根据部署ID查询流程图")
    @PostMapping("/definition/getProcessImg")
    public R<String> getProcessImageBase64(@RequestBody ProcessDefinitionRequest request) throws IOException {
        return R.success(processDefinitionService.getProcessImageBase64(request.getDeploymentId()));
    }

    /**
     * 将流程定义转模型
     *
     * @param request request
     * @return R<Void>
     */
    @ApiOperation(value = "将流程定义转模型")
    @PostMapping("/definition/convertToModel")
    public R<Void> convertToModel(@RequestBody ProcessDefinitionRequest request) throws UnsupportedEncodingException, XMLStreamException {
        processDefinitionService.convertToModel(request.getId());
        return R.success();
    }

    /**
     * 删除流程定义
     *
     * @param request request
     * @return R
     */
    @ApiOperation(value = "删除流程定义")
    @PostMapping("/definition/deleteProcessDeploymentByIds")
    public R<Void> deleteProcessDeploymentByIds(@RequestBody ProcessDefinitionRequest request) throws Exception {
        processDefinitionService.deleteProcessDeploymentByIds(request.getDeploymentId());
        return R.success();
    }


    /**
     * 导入流程定义
     */
    @Log(title = "导入流程定义", businessType = BusinessType.INSERT)
    @PostMapping("/definition/upload")
    @ApiOperation(value = "导入流程定义")
    public R<Void> upload(@RequestParam("file") MultipartFile file) throws IOException {
        processDefinitionService.deployProcessDefinition(file);
        return R.success();
    }


}
