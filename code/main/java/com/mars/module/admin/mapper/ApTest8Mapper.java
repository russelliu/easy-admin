package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApTest8;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 测试8Mapper接口
 *
 * @author mars
 * @date 2024-01-27
 */
public interface ApTest8Mapper extends BasePlusMapper<ApTest8> {

}
