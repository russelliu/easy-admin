package com.mars.common.response.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-31 15:49:04
 */
@Data
@ApiModel(value = "获取系统公共数据")
public class CommonDataResponse {

    @ApiModelProperty(value = "uuid")
    private String uuid;

    @ApiModelProperty(value = "图片地址")
    private String img;

}
