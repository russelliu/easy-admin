package com.mars.module.workflow.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 审批人员对象 process_node_assignee
 *
 * @author mars
 * @date 2024-01-25
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "审批人员对象")
@Builder
@Accessors(chain = true)
@TableName("process_node_assignee")
public class ProcessNodeAssignee extends BaseEntity {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 流程key
     */
    @Excel(name = "流程key")
    @ApiModelProperty(value = "流程key")
    private String flowKey;
    /**
     * 流程节点ID
     */
    @Excel(name = "流程节点ID")
    @ApiModelProperty(value = "流程节点ID")
    private String nodeId;
    /**
     * 审批人变量
     */
    @ApiModelProperty(value = "审批人变量")
    private String assigneeVariable;
    /**
     * 关联ID
     */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String assignee;
    /**
     * 类别
     */
    @Excel(name = "类别")
    @ApiModelProperty(value = "类别")
    private Integer assigneeType;
}
