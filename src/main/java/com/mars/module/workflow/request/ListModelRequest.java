package com.mars.module.workflow.request;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.activiti.engine.impl.persistence.entity.ModelEntityImpl;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2024-01-22 14:27:02
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ListModelRequest extends ModelEntityImpl {


    @ApiModelProperty(value = "当前页")
    private int pageNo = 1;

    @ApiModelProperty(value = "每页记录数")
    private int pageSize = 10;
}
