package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApTest8;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApTest8Request;

import java.util.List;

/**
 * 测试8接口
 *
 * @author mars
 * @date 2024-01-27
 */
public interface IApTest8Service {
    /**
     * 新增
     *
     * @param param param
     * @return ApTest8
     */
    ApTest8 add(ApTest8Request param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApTest8Request param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApTest8
     */
    ApTest8 getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApTest8>
     */
    PageInfo<ApTest8> pageList(ApTest8Request param);


    /**
     * 查询所有数据
     *
     * @return List<ApTest8>
     */
    List<ApTest8> list(ApTest8Request param);
}
