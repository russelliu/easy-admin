package com.mars.common.request.tool;

import lombok.Data;

import java.util.List;

/**
 * Api
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class ApiRequest {

    private String title;

    private String tags;

    private String url;

    private String method;

    private List<ApiParamRequest> requestList;

    private List<ApiParamRequest> responseList;


}
