package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 测试1对象 ap_test1
 *
 * @author mars
 * @date 2024-01-06
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "测试1对象")
@Builder
@Accessors(chain = true)
@TableName("ap_test1")
public class ApTest1 extends BaseEntity {

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 年龄
     */
    @Excel(name = "年龄")
    @ApiModelProperty(value = "年龄")
    private String age;
    /**
     * 性别
     */
    @Excel(name = "性别")
    @ApiModelProperty(value = "性别")
    private String gender;
}
