package com.mars.module.admin.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-21 21:39:45
 */
@Data
@ApiModel(value = "登录系统配置")
public class SysConfigResponse {

    @ApiModelProperty(value = "系统名称")
    private String systemName;

    @ApiModelProperty(value = "版权名称")
    private String copyright;

    @ApiModelProperty(value = "标语")
    private String slogan;

    @ApiModelProperty(value = "公钥")
    private String publicKey;

    @ApiModelProperty(value = "是否允许注册")
    private boolean registerSwitch;
}
