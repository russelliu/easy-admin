package com.mars.module.workflow.service;

import com.mars.common.response.PageInfo;
import com.mars.module.workflow.entity.ProcessDefinition;
import com.mars.module.workflow.request.ListModelRequest;
import com.mars.module.workflow.request.ModelRequest;
import com.mars.module.workflow.request.ProcessDefinitionRequest;
import org.activiti.engine.repository.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 流程定义接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2024-01-22 17:14:10
 */
public interface IProcessDefinitionService {

    /**
     * 分页查询流程定义
     *
     * @return PageInfo<ProcessDefinition>
     */
    PageInfo<ProcessDefinition> pageList(ProcessDefinitionRequest request);

    /**
     * @param deploymentId deploymentId
     * @return String
     */
    String getProcessImageBase64(String deploymentId) throws IOException;

    /**
     * 流程定义状态变更
     *
     * @param id           ID
     * @param suspendState suspendState
     */
    void suspendOrActiveApply(String id, String suspendState);

    /**
     * 将流程定义转模型
     *
     * @param processDefinitionId processDefinitionId
     */
    void convertToModel(String processDefinitionId) throws UnsupportedEncodingException, XMLStreamException;

    /**
     * 删除流程定义
     *
     * @param deploymentId deploymentId
     */
    void deleteProcessDeploymentByIds(String deploymentId) throws Exception;


    void deployProcessDefinition(MultipartFile file) throws IOException;
}
