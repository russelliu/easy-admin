package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApTest8Request;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.ApTest8Mapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.ApTest8;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IApTest8Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 测试8业务层处理
 *
 * @author mars
 * @date 2024-01-27
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApTest8ServiceImpl implements IApTest8Service {

    private final ApTest8Mapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApTest8 add(ApTest8Request request) {
        ApTest8 entity = ApTest8.builder().build();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ApTest8Request request) {
        ApTest8 entity = ApTest8.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApTest8 getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApTest8> pageList(ApTest8Request request) {
        Page<ApTest8> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApTest8> query = this.buildWrapper(request);
        IPage<ApTest8> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<ApTest8> list(ApTest8Request request) {
        LambdaQueryWrapper<ApTest8> query = this.buildWrapper(request);
        return baseMapper.selectList(query);
    }

    private LambdaQueryWrapper<ApTest8> buildWrapper(ApTest8Request param) {
        LambdaQueryWrapper<ApTest8> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(ApTest8::getName ,param.getName());
        }
         if (StringUtils.isNotBlank(param.getAge())){
               query.like(ApTest8::getAge ,param.getAge());
        }
         if (StringUtils.isNotBlank(param.getGender())){
               query.like(ApTest8::getGender ,param.getGender());
        }
        return query;
    }

}
