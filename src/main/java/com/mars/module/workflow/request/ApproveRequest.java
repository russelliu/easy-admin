package com.mars.module.workflow.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2024-01-26 13:59:13
 */
@Data
@ApiModel(value = "审批请求参数")
public class ApproveRequest {

    private static final long serialVersionUID = 1371536839623955109L;

    /**
     * 业务单据ID
     */
    @ApiModelProperty(value = "业务单据ID")
    private String businessKey;

    @NotBlank(message = "流程实例ID不能为空")
    @ApiModelProperty(value = "流程实例ID", required = true)
    private String instanceId;

    @ApiModelProperty(value = "当前处理人", required = true)
    private String applyUser;

    @NotBlank(message = "任务ID不能为空")
    @ApiModelProperty(value = "任务id", required = true)
    private String taskId;

    @NotNull(message = "请选择审批类型")
    @ApiModelProperty(value = "审批意见类型 1 同意 2 驳回")
    private Integer opinionType;

    @ApiModelProperty(value = "审批意见")
    private String opinionContent;

    @ApiModelProperty(value = "流程变量")
    private Map<String, Object> variables;

}
