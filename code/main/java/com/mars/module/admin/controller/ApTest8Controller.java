package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.ApTest8;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApTest8Service;
import com.mars.module.admin.request.ApTest8Request;

import javax.servlet.http.HttpServletResponse;

/**
 * 测试8控制层
 *
 * @author mars
 * @date 2024-01-27
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "测试8接口管理",tags = "测试8接口管理")
@RequestMapping("/admin/apTest8" )
public class ApTest8Controller {

    private final IApTest8Service iApTest8Service;

    /**
     * 分页查询测试8列表
     */
    @ApiOperation(value = "分页查询测试8列表")
    @PostMapping("/pageList")
    public R<PageInfo<ApTest8>> pageList(@RequestBody ApTest8Request apTest8Request) {
        return R.success(iApTest8Service.pageList(apTest8Request));
    }

    /**
     * 获取测试8详细信息
     */
    @ApiOperation(value = "获取测试8详细信息")
    @GetMapping(value = "/query/{id}")
    public R<ApTest8> detail(@PathVariable("id") Long id) {
        return R.success(iApTest8Service.getById(id));
    }

    /**
     * 新增测试8
     */
    @Log(title = "新增测试8", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增测试8")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ApTest8Request apTest8Request) {
        iApTest8Service.add(apTest8Request);
        return R.success();
    }

    /**
     * 修改测试8
     */
    @Log(title = "修改测试8", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改测试8")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ApTest8Request apTest8Request) {
        iApTest8Service.update(apTest8Request);
        return R.success();
    }

    /**
     * 删除测试8
     */
    @Log(title = "删除测试8", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除测试8")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iApTest8Service.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
    * 导出测试8
    *
    * @param response response
    * @throws IOException IOException
    */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody ApTest8Request apTest8Request) throws IOException {
        List<ApTest8> list = iApTest8Service.list(apTest8Request);
        ExcelUtils.exportExcel(list, ApTest8.class, "测试8信息", response);
    }
}
